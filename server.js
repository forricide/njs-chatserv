const express = require('express');
const bp = require('body-parser');
var app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bp.urlencoded({ extended: true }));

var chathistory = [];

app.get('/', function (req, res) {
    var date = new Date();
    var current_time = date.getHours() + ':' + date.getMinutes();
    res.render('index', {chathistory, error: null, name: 'user'+Math.floor(Math.random()*1000), current_time});
});

app.post('/', function (req, res) {
    var date = new Date();
    var current_time = date.getHours() + ':' + date.getMinutes();
    if (req.body.chatbox != undefined) chathistory.push(req.body.chatbox);
    let name = req.body.namebox;
    if (name == undefined) name = 'user'+Math.floor(Math.random()*1000);
    let err = false;
    if (Math.floor(Math.random() * 4) == 1) {
        err = true;
    }


    if (err) res.render('index', {chathistory, error: 'Error: Please try again!', name: name, current_time});
    else {
        res.render('index', {chathistory, error: null, name: name, current_time});
    }
});

app.listen(34040, function() {
    console.log('Listening on port 34040.');
});
